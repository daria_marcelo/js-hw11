const btn = document.querySelector('button');
const eyes = document.querySelectorAll('.icon-password');
const inputPassword = document.querySelector('.enter-password');
const inputRepeatPassword = document.querySelector('.repeat-password');
errorText = document.querySelector('.error')



function visiblePassword() {

    if (!eyes.length) {
        return;
    }

    eyes.forEach(eye => {
        eye.addEventListener('click', (e) => {
            e.preventDefault()

            const input = eye.previousElementSibling;
            if (!input) {
                return;
            }

            if (input.getAttribute('type') === 'password') {
                input.setAttribute('type', 'text')
                eye.classList.add('fa-eye-slash');
                eye.classList.remove('fa-eye')
            } else {
                input.setAttribute('type', 'password');
                eye.classList.add('fa-eye');
                eye.classList.remove('fa-eye-slash')
            }
        });
    })
}
function passwordCheck (){
    btn.addEventListener('click',()=>{
        if (inputPassword.value === inputRepeatPassword.value){
            alert('You are welcome')
        }
        else
        {
            errorText.style.display = 'block';
        }
    })
}
visiblePassword();
passwordCheck();



